# MCU-SwissSolarBoat
written by : Baptiste Savioz, the 03.03.2022  
reviewed by : Reuné Beuchat, the 03.04.2022  
updaten by : ..., the .. .. ...  

# Introduction
This repository contains all required files and information for complete characterization of the main MCU-board of Swiss Solar Boat. Please read the documentation carefully before starting, especially the _Important things before starting_ section.

The main MCU-board is made of a powerfull microcontroller used for communication (based on the CAN protocol) between the differents modules of the boat. It also includes I2C lines with dedicated connectors for communication insides boat modules (eg. the battery box), a SD card holder to save a logfile for example, a general purpose switch and some LEDs. Furthermore, it comes with the possibility to mount a shield on it, thus enhacing it's capabilities.

The complete project has been developed in Altium environnement. All the files of the project are provided and you can open the project in altium for further development. The schematic is easily available in PDF format at [in the altium files folder](MCU-main-board-V2/MCU-main-board-V2.pdf)

## Important things before starting

- Please read _Power source selection_ chapter otherwise you may damage the board, your computer or even the boat.
- The dedicated documentation of shields developped for the main MCU-board should be in their respective folder.

# How to use the board and program it (guide for unexperienced user)
|MCU Pin|Arduino Pin Name|Function|comment|
|-------|----------------|------|----|
|PA02|PA02 / A0 / DAC0|----|----|
|PA03|PA03 / A5 / AREF|----|reference voltage for ADC can be choosed from that pin, but never higher than 3.3V|
|PB04|PB04 / SWITCH_BUILTIN|----|----|
|PB05|PB05 / LED_BUILTIN|----|----|
|PB06|PB06 |----|----|
|PB07|PB07|----|----|
|PB08|PB08 / A2|----|----|
|PB09|PB09 / A3|----|----|
|PA04|PA04 / A4|----|----|
|PA05|PA05 / A1 /DAC1|----|----|
|PA12|PA12 / SDA1|----|----|
|PA13|PA13 / SCL1|----|----|
|PB03|PB03 / A7|----|----|
|PB02|PB02|----|----|
|PB01|PB01 / A9|----|----|
|PB00|PB00 / A6|----|----|
|PB31|PB31|----|----|
|PB30|PB30|----|----|
|PA27|PA12|----|----|
|PB23|PB23|----|----|
|PB22|PB22|----|----|
|PA19|PA19|----|----|
|PA18|PA18|----|----|
|PA17|PA17 / SCL0|----|----|
|PA16|PA16 / SDA0|----|----|
|PB17|PB17 / LED_RX_CAN0|----|by default LED not mounted on the board|
|PB16|PB16 / LED_TX_CAN0|----|by default LED not mounted on the board|
|PB13|PB13 / LED_RX_CAN1|----|by default LED not mounted on the board|
|PB12|PB12 / LED_TX_CAN1|----|by default LED not mounted on the board|
|PA14|PA14 / FAULT_CAN1|----|----|
|PA15|PA15 / S_CAN1|----|----|
|PA20|PA20 / FAULT_CAN0|----|----|
|PA21|PA21 / S_CAN0|----|----|

# Technical Documentation

### Table of content
* Version History
* Power supply
* MCU
* Clock
* Communication
	* CAN
	* I2C
	* JTAG
	* USB
* Connectors
* SD card and memory
* Pinout and Shield
* Exhaustive list of component


### Version History
|Version|author|Date|
|-------|------|----|
|V1.1|Henry Papadatos|----|
|V1.2|Valentin Karam|----|
|V1.3|Yoann Lapijover|----|
|V2.1|Baptiste Savioz|March 2022|
|V2.2|Baptiste Savioz|April 2022|


## Sub-system characterization
### Power supply
The board is powered with a 24[V] DC line that goes all along the boat. It can be quite noisy and therefore we choosed to graduatly decreased the voltage from 24[V] to 5[V]  through a high efficiency buck converter and then from 5[V] to 3.3[V] with a LDO IC. The LDO will power only the main board to ensure stable and clean power to it. On the other hand, the 5[V] power supply can be used by the shield to power other small devices. 

 * **Buck converter** : 24[V] DC to 5[V] DC  
The buck used is the [TPS54233QDRQ1](https://www.ti.com/lit/ds/symlink/tps54233-q1.pdf?HQS=dis-mous-null-mousermode-dsf-pf-null-wwe&ts=1646666212563&ref_url=https%253A%252F%252Fwww.mouser.ch%252F) from texas instrument. It has been designed with WEBENCH software from TI to operate at 20-28[V] in, 5[V] out, 2[A] out max, 50[°C].  
https://webench.ti.com/power-designer/switching-regulator?powerSupply=0

![image](image/buck_design_webench.svg)

 * **LDO** : 5[V] DC to 3.3[V] DC    
 The LDO used is the [AP7365-33WG-7](https://www.diodes.com/assets/Datasheets/products_inactive_data/AP7365.pdf), which is a fixed output voltage LDO in the package SOT25. It not recommanded for new design and will shortly fall out of supply. The newer version AP7366 is available but haven't been choosen because parts weren't available for now. However, switching to AP7366 wouldn't required adaptation of the PCB since it is also in the SOT25 package. Further update of this PCB may want to upgrade the LDO to an up to date version.
 
 * **Power source selection** : 24V input and Vbus
 The board can be powered by 3 different sources : 24V from CAN connector 0, 24V from CAN connector 1, 5V from the USB-mini port.
 The 5V comming from the buck and the 5V comming from USB are each protected with diode. This should provided a _safe_ use of both source at the same time (The CAN connector is pluged and power comes in from the lowpower and a laptop is connected to the USB port). However, one should still be careful with ground consideration. For example, in the laptop is in charge, a ground may come from the charger wich may be different from the one provided by the lowpower from the boat. This could lead to damage in the board or the laptop.
 Moreover, one should select a power source between the two different CAN connector (power comming from CAN0 or CAN1 connector). There is a double pole double throw switch (DPDT) dedicated for this. Please note that therefore 24V and Gnd comming from the two CAN connectors aren't connected. Common ground should be therefore ensure somewhere else. Though it is possible to solder a bridge if the application needs to connect ground reference and power supply to both CAN connector but in this case, the board should be specificaly marqued in order not to be mistaken because missused can lead to permanent damage. In doubt of correct use, please ask for help. 
 
 ### MCU
The board is based on [ATSAME51J20A-AF](https://www.mouser.ch/datasheet/2/268/60001507E-1660031.pdf) MCU from atmel, using the 32-bit ARM® Cortex®-M4 processor with Floating Point Unit (FPU). It is running at 120 [MHz] on 3.3[V].
* **Reset Pin** the reset pin is low active and powered-up trough a low pass RC filter as recommanded for design in noisy environnement. Moreover, it's connected to a push button for manual reset.
* **Power Pins** The power pins are wired according to typical power connection for switching mode power supply, with a feartite bead for high frequency isolation and EMC.
![image](image/power_pins_typical.png)

### Clock
The MCU has several clocking options and can admit two external oscillators (32768[Hz] for real time counting applications RTC and higher frequency oscillators). By default it is running at 48[MHz] on internal oscillators. Only the RTC oscillators is mounted on the board. It is a 32768[Hz] oscillators with low load capacitance (9[pF]) [Q 0,032768-JTX310-9-20-T1-HMR-LF](https://www.jauch.com/downloadfile/57fde22d50dbf_d3c203011c87952f2834/jtx310-auto-2-210512.pdf) connected to pin PA0 and PA1 (XIN32, XOUT32).
Hz
### Communication
* **CAN** : The MCU comes with two built-in CAN controller which will be both used. The CAN tranciever [TCAN337GDR](https://www.ti.com/lit/ds/symlink/tcan337g.pdf?ts=1646566581440&ref_url=https%253A%252F%252Fwww.ti.com%252Fstore%252Fti%252Fen%252Fp%252Fproduct%252F%253Fp%253DTCAN337GDR) is used to handle the CAN protocol. It's 3.3V logic, therefore the differential on the CAN bus is 3.3V. Nonetheless, the device is fully compatible with 5V differential logic. The CAN tranciver uses a serial line to communicate with the CAN controller of the MCU. Moreover it also inlcudes a silent mode for listen only (high active) and a fault pin to indicate a failure on the device (high active). Furthermore, the CAN lines are available through a [JST-S04B-PASK-2-LF-SN](https://datasheet.octopart.com/S04B-PASK-2%28LF%29%28SN%29-JST-datasheet-1807.pdf) connector (4 pins - 2mm pitch). One must be extra careful when chosing the power source.

|Name|Pin connected on the MCU|comment|
|----|------------------------|-------|
|RX_CAN0|PA23||
|TX_CAN0|PA22||
|S_CAN0|PA21|a pull down resistor is mounted to drive the line low when it isn't actively driven by the MCU|
|FAULT_CAN0|PA20||
|RX_CAN1|PB15||
|TX_CAN1|PB14||
|S_CAN1|PA15|a pull down resistor is mounted to drive the line low when it isn't actively driven by the MCU|
|FAULT_CAN1|PA14||

* **I2C** : The MCU comes with 8 SERCOM lines, that can be configured for serial communication protocol such as UART, I2C, RS232, RS485, SPI. Please carefuly check the documentation before configuring one of these protocol, they require dedicated SERCOM port. Section 6.2.8.1 may be handy. Moreover, not all SERCOM port can be used for I2C communication. The pins that support I2C communication for a 64 pins package are :  
![image](image/I2C_pinout.png)
the MCU-board comes with one dedicated I2C line. Another line has been left free for that pruposes but there is no connector mounted on the board by default. Other I2C pins than those listed below are already used for other tasks (CAN, SD-card) and aren't available anymore.

|Name|Pin connected on the MCU|comment|
|----|------------------------|-------|
|SDA_0|PA16|a 3.9k pull up resistor is mounted on the board|
|SCL_0|PA17|a 3.9k pull up resistor is mounted on the board|  
|SDA_1|PA13|*no connector mounted on the main-MCU board, a dedicated shield is necessary to use that line*|
|SCL_1|PA12|*no connector mounted on the main-MCU board, a dedicated shield is necessary to use that line*|  

The I2C line 0 is available through a [JST-S04B-PASK-2-LF-SN](https://datasheet.octopart.com/S04B-PASK-2%28LF%29%28SN%29-JST-datasheet-1807.pdf) connector (4 pins - 2mm pitch), with 3.3V power supply and ground comming with. **Moreover, one must be careful when connecting if the device one try to connect to has it's own power source**. Please mind, if your plan to use the I2C1 line that pull up resistor are required by the MCU in order to work properly (datasheet p. 1006). 

* **SWD** : The MCU has a SWD debugging standard built-in. The connections is made through a 10 (2x5) male pin headers connector with 1.27[mm] pitch : [FTSH-105-01-L-DV-K](http://suddendocs.samtec.com/catalog_english/ftsh_smt.pdf).  
<img src="/image/JTAG_pins.png" alt="JTAG_oins"
	title="JTAG_pins" width="600" height="400" />

The MCU can easily be programmed through this SWD interface with for example a JLINK-EDU-MINI. Please mind that the SWD protocol don't carry any power supply line and therefore the board needs to be powered from another source (USB for example).
 
* **USB** : the MCU has a built-in USB 2.0 line. In order to be used the MCU need to be powered at 3.3[V] (min 3[V] - max 3.6[V]) and there is some restrictions on clock source and frequency, especially for USB host connections, that need to be taken into account when doing the firmware. A micro-USB B connector has been choosen for that connection. The VBUS line (5[V]) is connected to the power supply of the board through a schottky diode, which should prevent any possible damage to the laptop if the board is already powered from another source (CANx connector). However, extra considerations regarding to ground need to be taken (for example if the laptop is charging and thus connected to household ground). The ID pin from the micro-USB B connector is by default left unconnected but a zero ohm resistor can me mounted on the board (by default not installed) if it needs to be grounded (in order to host USB communication).
![image](image/usb_schematic.png)

### Connectors
- CAN connectors are JST PA 2mm pitch 4 pin
- I2C connector is JST PA 2mm pitch 4 pin
- Micro-USB type B
- JTAG 10 (2x5) male pin headers connector with 1.27[mm] pitch : [FTSH-105-01-L-DV-K](http://suddendocs.samtec.com/catalog_english/ftsh_smt.pdf). 
- Shield is made of 2 time 1 row, 25 position through hole female pin header of 2.54mm pitch.

### SD-card and memory
An SD slot is available on the board and it is handle by the SDHC0 line of the MCU. It's powered at the 3.3[V] by the LDO. Please be careful and read the dedicated documentation when designing an SD application, especially about the oprating voltage.

|Name|Pin connected on the MCU|comment|
|----|------------------------|-------|
|SDHC0 DAT0|PA09||
|SDHC0 DAT1|PA10||
|SDHC0 DAT2|PA11||
|SDHC0 DAT3|PB10||
|SDHC0 CMD|PA08||
|SDHC0 CLK|PB11||
|SDHC0 CD|PA06||
|SDHC0 WP|PA07||


## Pinout and Shield
The MCU-board has been designed to connect a shield on its bottom. The connectors are classic female pin headers with a pitch of 2.54mm.  There are two row of 20 pins on each side of the board. Please take care when designing a shield that the connectors are mounted on the bottom side of the MCU-board. 

Schematic Pinout|Dimensions and placement on the PCB (**!bottom view!**)
-|- 
![alt](image/pinout.png)|![alt](image/dimension_pinout_PCB.png)

Top view|Bottom view
-|- 
![alt](image/PCB_top_view.png)|![alt](image/PCB_bottom_view.png)


## exhaustive list of component

|Name                             |Description                                                                                                                       |Designator                                      |Quantity|Manufacturer Part Number 1|
|---------------------------------|----------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------|--------|--------------------------|
|TPS54233QDRQ1                    |Integrated Circuit                                                                                                                |Buck converter : 24V to 3.3V                    |1       |                          |
|Capacitor 10µF +/-20% 25V 0805   |Chip Capacitor, 10µF +/-20%, 25V, 0805, Thickness 1.45 mm                                                                         |C1, C20, C21                                    |3       |GRM21BR61E106KA73L        |
|Capacitor 10 pF +/- 5% 50 V 0603 |Chip Capacitor, 10 pF, +/- 5%, 50 V, 0603 (1608 Metric)                                                                           |C2                                              |1       |CC0603JRNPO9BN100         |
|06035C102KAT2A                   |General Purpose Ceramic Capacitor, 0603, 1nF, 10%, X7R, 15%, 50V                                                                  |C3                                              |1       |06035C102KAT2A            |
|CL10B822KB8NNNC                  |Cap Ceramic 8.2nF 50V X7R ±10% Pad SMD 0603 +125°C T/R                                                                            |C4                                              |1       |CL10B822KB8NNNC           |
|GRM32ER61C476KE15L               |CAP CER 47UF 16V X5R 1210                                                                                                         |C5, C6, C7, C22                                 |4       |GRM32ER61C476KE15L        |
|Capacitor 1µF +/-40% 50V 0603    |Chip Capacitor, 1µF +/-40%, 50V, 0603, Thickness 1 mm                                                                             |C8, C9, C15                                     |3       |CL10A105KB8NNNC           |
|CC0603JRNPO9BN180                |Chip Capacitor, 18 pF, +/- 5%, 50 V, -55 to 125 degC, 0603 (1608 Metric), RoHS, Tape and Reel                                     |C10, C11                                        |2       |CC0603JRNPO9BN180         |
|Capacitor 100 nF +/-10% 50 V 0603|Chip Capacitor, 100 nF, +/- 10%, 50 V, 0603 (1608 Metric)                                                                         |C12, C13, C14, C16, C17, C18, C23, C24, C25, C26|10      |CC0603KRX7R9BB104         |
|Capacitor 4.7µF +/-40% 25V 0805  |Chip Capacitor, 4.7µF +/-40%, 25V, 0805, Thickness 1.45 mm                                                                        |C19                                             |1       |08053D475MAT2A            |
|TCAN337GDR                       |3.3-V CAN Transceivers with CAN FD (Flexible Data Rate) 8-SOIC -40 to 125                                                         |CAN tranciever 0, CAN tranciever 1              |2       |TCAN337GDR                |
|SS34-E3/57T                      |DIODE SCHOTTKY 40V 3A DO214AB                                                                                                     |D1                                              |1       |SS34-E3/57T               |
|BLM18SG221TN1D                   |Chip Ferrite Bead, 0603, 220Ω @ 100MHz, 25%, 0.04Ω, 2.5A                                                                          |FB1                                             |1       |BLM18SG221TN1D            |
|Can connector                    |S04B-PASK-2-LF-SN                                                                                                                 |J1, J2, J3, J4                                  |4       |S04B-PASK-2(LF)(SN)       |
|105017-0001                      |Micro-USB B Receptacle, Right Angle, Bottom Mount, Surface Mount, with Solder Tabs, -30 to 85 degC, 5-Pin USB, RoHS, Tape and Reel|J5                                              |1       |1050170001                |
|I2C connector                    |S04B-PASK-2-LF-SN                                                                                                                 |J6                                              |1       |S04B-PASK-2(LF)(SN)       |
|ESQ-120-14-G-S                   |CONN SOCKET 20POS 0.1 GOLD PCB                                                                                                    |J7, J8                                          |2       |ESQ-120-14-G-S            |
|FTSH-105-01-L-DV-K               |Male Header, Pitch 1.27 mm, 2 x 5 Position, Height 6.25 mm                                                                        |JTAG                                            |1       |FTSH-105-01-L-DV-K        |
|7447709330                       |SMD-Shielded Power Inductor WE-PD, L = 33.0 µH                                                                                    |L1                                              |1       |7447709330                |
|CBC3225T100MR                    |SMD Inductors 10uH ±20% 0.9A 0.1729Ω 1210                                                                                         |L2                                              |1       |CBC3225T100MR             |
|AP7365-33WG-7                    |IC REG LDO 3.3V 0.6A SOT-25                                                                                                       |LDO 5V to 3.3V                                  |1       |AP7365-33WG-7             |
|ATSAME51J20A-AF                  |Integrated Circuit                                                                                                                |Microcontrolleur                                |1       |                          |
|22-28-4033                       |Male Header, Pitch 2.54 mm, 1 x 3 Position, Height 8.38 mm, Tail Length 3.18 mm, RoHS, Bulk                                       |P1, P2                                          |2       |22-28-4033                |
|61900211121                      |Male Locking Header WR-WTB, THT, Vertical, pitch 2.54 mm, 1 x 2 position                                                          |P3                                              |1       |61900211121               |
|CRCW0603169KFKEA                 |RES Thick Film, 169kΩ, 1%, 0.1W, 100ppm/°C, 0603                                                                                  |R1                                              |1       |CRCW0603169KFKEA          |
|RR0816P-1962-D-29C               |19.6kΩ ±0.5% 0.0625W 1/16W Chip Resistor 0603 (1608 Metric) Thin Film                                                             |R2                                              |1       |RR0816P-1962-D-29C        |
|ERJ-3EKF5232V                    |                                                                                                                                  |R3                                              |1       |ERJ-3EKF5232V             |
|CRCW060310K2FKEA                 |RES Thick Film, 10.2kΩ, 1%, 0.1W, 100ppm/°C, 0603                                                                                 |R4                                              |1       |CRCW060310K2FKEA          |
|RR0816P-1961-D-29H               |1.96kΩ ±0.5% 0.0625W 1/16W Chip Resistor 0603 (1608 Metric) Thin Film                                                             |R5                                              |1       |RR0816P-1961-D-29H        |
|Resistor 10k +/-1% 0603 100 mW   |Chip Resistor, 10 KOhm, +/- 1%, 01 W, -55 to 155 degC, 0603 (1608 Metric)                                                         |R6, R8, R9, R10, R11, R12, R13, R14, R15, R16   |10      |RC0603FR-0710KL           |
|104031-0811                      |Micro SD Card, RA, -25 to 85 degC, 8-Pin SMD, RoHS, Tape and Reel                                                                 |SD1                                             |1       |1040310811                |
|custom Solder bridge             |custom solder bridge                                                                                                              |SJ1, SJ2, SJ3, SJ4, SJ5, SJ6, SJ7               |7       |                          |
|FSMSM                            |FSMSM Push Button Switch, 50 mA, -35 to 85 degC, 2-Pin SMD, RoHS, Bulk                                                            |SW1                                             |1       |FSMSM                     |
|ABS07-166-32.768KHZ-T            |Crystal 32.768kHz 10ppm 7pF -40C +85C                                                                                             |X1                                              |1       |ABS07-166-32.768KHZ-T     |


 Repository for documentation, characteristics and all required elements for the Microcontroller board, including PCB conception, of SSB.
